package me.abdalion.mcleverbot;

import java.util.logging.Logger;

import me.abdalion.Utils.ChatterBot;
import me.abdalion.Utils.ChatterBotFactory;
import me.abdalion.Utils.ChatterBotSession;
import me.abdalion.Utils.ChatterBotType;
import me.abdalion.command.MCleverbotCommandExecutor;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MCleverbot extends JavaPlugin {

	@Override
	public void onDisable() {
		getLogger().info("Is shutting down!");
	}

	public final Logger logger = Logger.getLogger("Minecraft");
	public static ChatterBotSession botsession;

	@Override
	public void onEnable() {
	    PluginManager pm = getServer().getPluginManager();
		MCleverbotCommandExecutor mcleverbot = new MCleverbotCommandExecutor(this);
		getCommand("mcleverbot").setExecutor(mcleverbot);
		initializeBot();
		
	}
	public void initializeBot() {
		ChatterBotFactory factory = new ChatterBotFactory();
		ChatterBot bot1 = null;
		try {
			bot1 = factory.create(ChatterBotType.CLEVERBOT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ChatterBotSession bot1session = bot1.createSession();
		botsession = bot1session;
	}
	
}
