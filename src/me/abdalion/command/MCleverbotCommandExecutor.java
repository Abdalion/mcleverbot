package me.abdalion.command;

import me.abdalion.Utils.ChatterBot;
import me.abdalion.Utils.ChatterBotFactory;
import me.abdalion.Utils.ChatterBotSession;
import me.abdalion.Utils.ChatterBotType;
import me.abdalion.mcleverbot.MCleverbot;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MCleverbotCommandExecutor implements CommandExecutor {

	private MCleverbot plugin;

	public MCleverbotCommandExecutor(MCleverbot plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (label.equalsIgnoreCase("mcleverbot")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.GRAY + "[MCleverbot] Plugin credit: Abdalion");
				sender.sendMessage(ChatColor.GRAY + "[MCleverbot] Bot API credit: pierredavidbelanger (GitHub)");
				sender.sendMessage(ChatColor.GOLD + "Usage: MCleverbot message");
			}
			else if(args.length > 0) {
				String message = concat(args);
				try {
					sender.sendMessage(MCleverbot.botsession.think(message));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
				
		}
		return false;
	}

	public String concat(String[] args) {
		  String result = "";
		  for (String arg : args) {
		    result += arg;
		  }
		  return result;
		}
}
